from django.shortcuts import render
from django.http import HttpResponse, FileResponse, JsonResponse
import zipfile, io
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
import os, glob, requests, json
from .models import ToBeCompressed


# Create your views here.
PATH_TO_UPLOADED_FILES_FOLDER = os.path.join(settings.BASE_DIR, 'uploaded-files', 'uploads/')
PATH_TO_UPLOADED_FILES_FOLDER_WITHOUT_UPLOADS = os.path.join(settings.BASE_DIR, 'uploaded-files/')
PATH_TO_ZIPPED_FILES_FOLDER = os.path.join(settings.BASE_DIR, 'zip_output/')

def createZipFile(file_name, file_name_with_zip_extension):
    zipfile_object = zipfile.ZipFile(PATH_TO_ZIPPED_FILES_FOLDER + file_name_with_zip_extension, 'w', zipfile.ZIP_DEFLATED)
    zipfile_object.write(PATH_TO_UPLOADED_FILES_FOLDER + file_name, arcname=file_name)
    zipfile_object.close()

    zipped_file = open(PATH_TO_ZIPPED_FILES_FOLDER + file_name_with_zip_extension, 'rb')
    return zipped_file

def cleaningUp():
    ToBeCompressed.objects.all().delete()
    uploaded_files = glob.glob(PATH_TO_UPLOADED_FILES_FOLDER + '*')
    zipped_files = glob.glob(PATH_TO_ZIPPED_FILES_FOLDER + '*')

    for uploaded_file in uploaded_files:
        os.remove(uploaded_file)
    
    for zipped_file in zipped_files:
        os.remove(zipped_file)

def ambil_token(user, pasw, client_id, client_secret):
    URL = "http://oauth.infralabs.cs.ui.ac.id/oauth/token"
    payload = {'username': user, 'password': pasw, 'grant_type': 'password', 'client_id': client_id, 'client_secret': client_secret}
    r = requests.post(url = URL, data=payload)
    return json.loads(r.text)

def ambil_resource(access_token):
    URL = "http://oauth.infralabs.cs.ui.ac.id/oauth/resource"
    data_headers = { 'Authorization': 'Bearer ' + access_token }
    r = requests.get(url = URL, headers = data_headers)
    return json.loads(r.text)

@csrf_exempt
def controller(request):
    cleaningUp()
    if request.method == 'POST':
        params = areParametersCompleted(request.POST)
        if (len(params) != 0):
            message = generateIncompleteParametersMessage(params)
            return JsonResponse(jsonErrorMessage(message))
        else:
            generated_token = ambil_token(request.POST['username'], request.POST['password'], request.POST['client_id'], request.POST['client_secret'])

            if 'error' in generated_token:
                return JsonResponse(generated_token, safe=False)
            acquired_resources = ambil_resource(generated_token['access_token'])

            if 'error' in acquired_resources:
                return JsonResponse(acquired_resources, safe=False)
            return compressFile(request.FILES)
    else:
        return JsonResponse(jsonErrorMessage('untuk melakukan kompresi file, gunakan method POST'))


def areParametersCompleted(request_body):
    incomplete_params = []
    required_fields = ['username', 'password', 'client_id', 'client_secret']
    for required_field in required_fields:
        if required_field not in request_body:
            incomplete_params.append(required_field)
    return incomplete_params

def generateIncompleteParametersMessage(list_of_incomplete_params):
    string_response = 'parameter yang dibutuhkan untuk OAUTH tidak lengkap. Parameter yang kurang : '
    for incomplete_param in list_of_incomplete_params:
        string_response = string_response + incomplete_param + ', '
    return string_response[:-2]

def compressFile(request_file):
    if 'file' not in request_file:
        return JsonResponse(jsonErrorMessage('File tidak ditemukan. Silakan unggah satu file.'))
    inbody_file = request_file['file']
    create_object = ToBeCompressed.objects.create(upload=inbody_file)
    file_name = create_object.filename()
    file_name_with_zip_extension = file_name[:-4] + '.zip'
    zipped_file = createZipFile(file_name, file_name_with_zip_extension)
    return FileResponse(zipped_file)

def jsonErrorMessage(msg):
    return {'error': msg}
    