from django.db import models
import os
# Create your models here.

class ToBeCompressed(models.Model):
    upload = models.FileField(upload_to='uploads/')

    def filename(self):
        return os.path.basename(self.upload.name)